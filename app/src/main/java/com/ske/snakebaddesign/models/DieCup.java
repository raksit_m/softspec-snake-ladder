package com.ske.snakebaddesign.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Earth on 16/3/2559.
 */
public class DieCup {
    private List<Die> cup;
    private int sum;

    public DieCup() {
        cup = new ArrayList<Die>();
    }

    public int getValues() {
        sum = 0;
        for (Die die : cup) {
            sum += die.getValue();
        }

        return sum;
    }

    public List<Die> getCup() {
        return cup;
    }
}
