package com.ske.snakebaddesign.models;

/**
 * Created by Earth on 16/3/2559.
 */
public class Player {
    private Piece piece;
    private int color;

    public Player(Piece piece, int color) {
        this.piece = piece;
        this.color = color;
    }

    public void setColor(int newColor) {
        color = newColor;
    }

    public void setPosition(int newPosition) {
        piece.setPosition(newPosition);
    }

    public int getPosition() {
        return piece.getPosition();
    }

    public Piece getPiece() {
        return piece;
    }

    public int getColor() {
        return color;
    }
}
