package com.ske.snakebaddesign.models;

/**
 * Created by Earth on 16/3/2559.
 */
public class Piece {
    private int position;

    public Piece(int position) {
        this.position = position;
    }

    public void setPosition(int newPosition) {
        this.position = newPosition;
    }

    public int getPosition() {
        return position;
    }
}
