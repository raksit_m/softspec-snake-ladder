package com.ske.snakebaddesign.models;

import android.graphics.Color;

import com.ske.snakebaddesign.activities.GameActivity;
import com.ske.snakebaddesign.guis.BoardView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * Created by Earth on 16/3/2559.
 */
public class Game extends Observable {
    private static DieCup cup;
    private List<Player> players;

    private int[] colors;
    private int boardSize;
    private int numPlayers;
    private int turn;

    public Game(int numPlayers) {
        cup = new DieCup();
        this.numPlayers = numPlayers;
        players = new ArrayList<Player>();
        colors = new int[7];

        colors[0] = Color.RED;
        colors[1] = Color.BLUE;
        colors[2] = Color.YELLOW;
        colors[3] = Color.GREEN;
        colors[4] = Color.BLACK;
        colors[5] = Color.WHITE;
        colors[6] = Color.MAGENTA;

        for(int i = 0; i < numPlayers; i++) {
            players.add(new Player(new Piece(0), colors[i]));
        }

        turn = 0;
        boardSize = 6;
    }

    public void addDie() {
        cup.getCup().add(new Die());
    }

    public int rollDie() {
        return cup.getValues();
    }

    public int adjustPosition(int current, int distance) {
        current = current + distance;
        int maxSquare = boardSize * boardSize - 1;
        if(current > maxSquare) {
            current = maxSquare - (current - maxSquare);
        }
        return current;
    }

    public void moveCurrentPiece(int value) {
        Player player = players.get(turn % numPlayers);
        player.setPosition(adjustPosition(player.getPosition(), value));
        turn++;
        this.setChanged();
        this.notifyObservers(GameActivity.class);
    }

    public String getWinnerText() {
        for(int i = 0; i < numPlayers; i++) {
            if(players.get(i).getPosition() == boardSize * boardSize - 1) {
                return "Player " + (i+1) + " Win";
            }
        }

        return "No Winner";
    }

    public void resetGame() {
        players.clear();
        turn = 0;

        for(int i = 0; i < numPlayers; i++) {
            players.add(new Player(new Piece(0), colors[i]));
        }
        this.setChanged();
        this.notifyObservers(GameActivity.class);

    }

    public int getCurrentPlayer() {
        return (turn % numPlayers) + 1;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public int getBoardSize() {
        return boardSize;
    }
}
